package com.shauncjones.materialtech.fluids;

import com.shauncjones.materialtech.fluids.blocks.BlockLiquidEndento;
import com.shauncjones.materialtech.fluids.items.ItemBucketEndento;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;

public class MTFluids {
	
	//Liquids
	public static Fluid liquidEndento;
	
	//Blocks
	public static Block blockLiquidEndento;
	
	//Items
	public static Item bucketEndento;
	
	public static void init(){
		
		liquidEndento = new Fluid("liquidEndento").setUnlocalizedName("liquidEndento").setLuminosity(10).setDensity(200).setTemperature(500).setViscosity(1000).setGaseous(false);
		
		FluidRegistry.registerFluid(liquidEndento);
		
		blockLiquidEndento = new BlockLiquidEndento(liquidEndento, Material.water);
		
		bucketEndento = new ItemBucketEndento(blockLiquidEndento).setContainerItem(Items.bucket).setUnlocalizedName("bucketEndento");
		
		GameRegistry.registerBlock(blockLiquidEndento, "blockLiqiudEndento");
		GameRegistry.registerItem(bucketEndento, "bucketEndento");
		
		FluidContainerRegistry.registerFluidContainer(liquidEndento, new ItemStack(bucketEndento), new ItemStack(Items.bucket));
		
	}

}
