package com.shauncjones.materialtech.fluids.items;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBucket;

public class ItemBucketEndento extends ItemBucket{

	public ItemBucketEndento(Block p_i45331_1_) {
		super(p_i45331_1_);
		this.setCreativeTab(CreativeTabs.tabMisc);
	}

}
