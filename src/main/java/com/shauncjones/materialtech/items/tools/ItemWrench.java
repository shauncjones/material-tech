package com.shauncjones.materialtech.items.tools;

import com.shauncjones.materialtech.MTech;

import net.minecraft.item.Item;

public class ItemWrench extends Item {
	
	public ItemWrench(String unlocalizedName){
		super();
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(MTech.MTMaterials);
	}

}
