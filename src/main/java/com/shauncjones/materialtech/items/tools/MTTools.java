package com.shauncjones.materialtech.items.tools;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

public class MTTools {
	
	public static Item Wrench;
	
	public static void init(){
		Wrench = new ItemWrench("wrench");
		GameRegistry.registerItem(Wrench, "wrench");
	}

}
