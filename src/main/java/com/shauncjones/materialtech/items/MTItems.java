package com.shauncjones.materialtech.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class MTItems {
	
	public static Item MaterialIngots;
	public static Item MaterialDusts;
	public static Item MaterialNuggets;
	
	
	public static void init(){		
		
		MaterialIngots = new ItemMaterialIngots("materialIngots");
		MaterialDusts = new ItemMaterialDusts("materialDusts");
		MaterialNuggets = new ItemMaterialNuggets("materialNuggets");
		
		GameRegistry.registerItem(MaterialIngots, "materialIngots");
		GameRegistry.registerItem(MaterialDusts, "materialDusts");
		GameRegistry.registerItem(MaterialNuggets, "materialNuggets");
		
		OreDictionary.registerOre("ingotCopper", new ItemStack(MaterialIngots, 0, 0));
		OreDictionary.registerOre("ingotTin", new ItemStack(MaterialIngots, 1, 1));
		OreDictionary.registerOre("ingotSilver", new ItemStack(MaterialIngots, 2, 2));
		OreDictionary.registerOre("materialSilicon", new ItemStack(MaterialIngots, 3, 3));
		OreDictionary.registerOre("ingotExplodium", new ItemStack(MaterialIngots, 4, 4));
		OreDictionary.registerOre("ingotEndento", new ItemStack(MaterialIngots, 5, 5));
		OreDictionary.registerOre("dustExplodium", new ItemStack(MaterialIngots, 3, 3));
		OreDictionary.registerOre("dustEndento", new ItemStack(MaterialIngots, 4, 4));
		
	}

}
