package com.shauncjones.materialtech.core;

import com.shauncjones.materialtech.MTech;
import com.shauncjones.materialtech.achieve.MTAchievements;
import com.shauncjones.materialtech.blocks.MTBlocks;
import com.shauncjones.materialtech.common.tile.TileEntitySlabChest;
import com.shauncjones.materialtech.core.listener.MTEventListener;
import com.shauncjones.materialtech.core.world.MTWorldGen;
import com.shauncjones.materialtech.fluids.MTFluids;
import com.shauncjones.materialtech.gui.MTGuiHandler;
import com.shauncjones.materialtech.handlers.BucketHandler;
import com.shauncjones.materialtech.items.MTItems;
import com.shauncjones.materialtech.items.tools.MTTools;
import com.shauncjones.materialtech.moddata.Refer;
import com.shauncjones.materialtech.recipes.MTCraftingSmelting;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {
	
	public void preInit(FMLPreInitializationEvent e) {
		
		MTBlocks.init();
		MTItems.init();
		MTTools.init();
		MTFluids.init();
		MTAchievements.init();
		MTCraftingSmelting.init();
		BucketHandler.INSTANCE.buckets.put(MTFluids.blockLiquidEndento, MTFluids.bucketEndento);
		MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);
		GameRegistry.registerWorldGenerator(new MTWorldGen(), 0);
		MinecraftForge.EVENT_BUS.register(new MTMobDropsHandler());
		GameRegistry.registerTileEntity(TileEntitySlabChest.class, Refer.MODID + "TileEntitySlabChest");
		NetworkRegistry.INSTANCE.registerGuiHandler(MTech.instance, new MTGuiHandler());

    }

    public void init(FMLInitializationEvent e) {

    	FMLCommonHandler.instance().bus().register(new MTEventListener());
    	
    }

    public void postInit(FMLPostInitializationEvent e) {

    }

}
