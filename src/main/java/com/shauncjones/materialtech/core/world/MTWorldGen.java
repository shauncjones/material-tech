package com.shauncjones.materialtech.core.world;

import java.util.Random;

import com.shauncjones.materialtech.blocks.MTBlocks;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;

public class MTWorldGen implements IWorldGenerator {
	
	private WorldGenerator copperOre;
	private WorldGenerator tinOre;
	private WorldGenerator silverOre;
	private WorldGenerator siliconOre;

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
	    switch (world.provider.dimensionId) {
	    case 0: //Overworld
	    	this.copperOre = new WorldGenMinable(MTBlocks.MaterialOres, 0, 8, Blocks.stone);
	    	this.tinOre = new WorldGenMinable(MTBlocks.MaterialOres, 1, 8, Blocks.stone);
	    	this.silverOre = new WorldGenMinable(MTBlocks.MaterialOres, 2, 8, Blocks.stone);
	    	this.siliconOre = new WorldGenMinable(MTBlocks.MaterialOres, 3, 3, Blocks.stone);
	    	this.runGenerator(this.copperOre, world, random, chunkX, chunkZ, 9, 0, 64);
	    	this.runGenerator(this.tinOre, world, random, chunkX, chunkZ, 9, 0, 64);
	    	this.runGenerator(this.silverOre, world, random, chunkX, chunkZ, 9, 0, 48);
	    	this.runGenerator(this.siliconOre, world, random, chunkX, chunkZ, 9, 0, 24);
	        break;
	    case -1: //Nether

	        break;
	    case 1: //End

	        break;
	    }
	}
	
	private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z, int chancesToSpawn, int minHeight, int maxHeight) {
	    if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
	        throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");

	    int heightDiff = maxHeight - minHeight + 1;
	    for (int i = 0; i < chancesToSpawn; i ++) {
	        int x = chunk_X * 16 + rand.nextInt(16);
	        int y = minHeight + rand.nextInt(heightDiff);
	        int z = chunk_Z * 16 + rand.nextInt(16);
	        generator.generate(world, rand, x, y, z);
	    }
	}

}
