package com.shauncjones.materialtech.core;

import com.shauncjones.materialtech.items.MTItems;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDropsEvent;

public class MTMobDropsHandler {
	
	@SubscribeEvent
	public void onMobDrops(LivingDropsEvent event){
		if (event.entity instanceof EntityCreeper){
			event.drops.clear();
	 
			ItemStack stack = new ItemStack(MTItems.MaterialDusts, 4);
			EntityItem drop = new EntityItem(event.entity.worldObj, event.entity.posX, event.entity.posY, event.entity.posZ, stack);
	 
			event.drops.add(drop);
		}
	}

}
