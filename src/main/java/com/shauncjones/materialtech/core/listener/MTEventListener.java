package com.shauncjones.materialtech.core.listener;

import com.shauncjones.materialtech.achieve.MTAchievements;
import com.shauncjones.materialtech.blocks.MTBlocks;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemPickupEvent;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class MTEventListener {
	
	@SubscribeEvent
	public void PlayerEvent(ItemCraftedEvent event){
		
		if(event.crafting.getItem() == Items.diamond_pickaxe){
			
			event.player.addStat(MTAchievements.DiamondPick, 1);
			
		}
	}
	
	@SubscribeEvent
	public void pickup(ItemPickupEvent event){
		
		if(event.pickedUp.getEntityItem().getItem() == Item.getItemFromBlock(MTBlocks.MaterialOres)){
			
			event.player.addStat(MTAchievements.MTStart, 1);
		}
	}
}
