package com.shauncjones.materialtech.recipes;

import com.shauncjones.materialtech.blocks.MTBlocks;
import com.shauncjones.materialtech.items.MTItems;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class MTCraftingSmelting {
	
	public static void init(){
		
		CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(new ItemStack(MTBlocks.MaterialBlocks, 0), "xxx", "xxx", "xxx", 'x', "ingotCopper"));
		CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(new ItemStack(MTBlocks.MaterialBlocks, 1), "xxx", "xxx", "xxx", 'x', "ingotTin"));
		CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(new ItemStack(MTBlocks.MaterialBlocks, 2), "xxx", "xxx", "xxx", 'x', "ingotSilver"));
		CraftingManager.getInstance().getRecipeList().add(new ShapelessOreRecipe(new ItemStack(MTBlocks.MaterialBlocks, 3), new Object[]{"materialSilicon", "materialSilicon", "materialSilicon", "materialSilicon"}));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(MTItems.MaterialIngots, 9, 0), new Object[]{"blockCopper"}));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(MTItems.MaterialIngots, 9, 1), new Object[]{"blockTin"}));
		
	}

}
