package com.shauncjones.materialtech.blocks.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.shauncjones.materialtech.MTech;
import com.shauncjones.materialtech.common.tile.TileEntitySlabChest;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSlabChest extends BlockContainer {
	
	private final Random rand = new Random();

	public BlockSlabChest(String unlocalizedName, Material p_i45386_1_) {
		super(p_i45386_1_);
		this.setBlockName(unlocalizedName);
		this.setCreativeTab(MTech.MTStorage);
		// TODO Auto-generated constructor stub
	}
	
	@Override
    //TODO:        renderAsNormalBlock()
    public boolean renderAsNormalBlock ()
    {
        return false;
    }

    //TODO:        isOpaqueCube()
    @Override
    public boolean isOpaqueCube ()
    {
        return false;
    }

    //TODO          shouldSideBeRendered
    @Override
    public boolean shouldSideBeRendered (IBlockAccess world, int x, int y, int z, int side)
    {
        if (side > 1)
            return super.shouldSideBeRendered(world, x, y, z, side);

        int meta = world.getBlockMetadata(x, y, z);
        boolean top = (meta | 8) == 1;
        if ((top && side == 0) || (!top && side == 1))
            return true;

        return super.shouldSideBeRendered(world, x, y, z, side);
    }

    //TODO:     addCollisionBoxesToList()
    @Override
    @SuppressWarnings("rawtypes")
    public void addCollisionBoxesToList (World world, int x, int y, int z, AxisAlignedBB axisalignedbb, List arraylist, Entity entity)
    {
        setBlockBoundsBasedOnState(world, x, y, z);
        super.addCollisionBoxesToList(world, x, y, z, axisalignedbb, arraylist, entity);
    }

    public void setBlockBoundsForItemRender ()
    {
        //TODO: setBlockBounds     
        setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
    }

    public void setBlockBoundsBasedOnState (IBlockAccess world, int x, int y, int z)
    {
        int meta = world.getBlockMetadata(x, y, z) / 8;
        float minY = meta == 1 ? 0.5F : 0.0F;
        float maxY = meta == 1 ? 1.0F : 0.5F;
        setBlockBounds(0.0F, minY, 0F, 1.0F, maxY, 1.0F);
    }

    public int onBlockPlaced (World par1World, int blockX, int blockY, int blockZ, int side, float clickX, float clickY, float clickZ, int metadata)
    {
        if (side == 1)
            return metadata;
        if (side == 0 || clickY >= 0.5F)
            return metadata | 8;

        return metadata;
    }

    public int damageDropped (int meta)
    {
        return meta % 8;
    }
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float lx, float ly, float lz){
		if (world.isRemote) return true;
	 
			TileEntity te = world.getTileEntity(x, y, z);
			if (te != null && te instanceof TileEntitySlabChest){
				player.openGui(MTech.instance, 0, world, x, y, z);
				return true;
			}
			return false;
	}
	 
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int par6){
		if (world.isRemote) return;
	 
		ArrayList drops = new ArrayList();
	 
		TileEntity teRaw = world.getTileEntity(x, y, z);
	 
		if (teRaw != null && teRaw instanceof TileEntitySlabChest){
			TileEntitySlabChest te = (TileEntitySlabChest) teRaw;
	 
			for (int i = 0; i < te.getSizeInventory(); i++){
				ItemStack stack = te.getStackInSlot(i);
	 
				if (stack != null) drops.add(stack.copy());
			}
	}
	 
	for (int i = 0;i < drops.size();i++){
			EntityItem item = new EntityItem(world, x + 0.5, y + 0.5, z + 0.5, (ItemStack)drops.get(i));
			item.setVelocity((rand.nextDouble() - 0.5) * 0.25, rand.nextDouble() * 0.5 * 0.25, (rand.nextDouble() - 0.5) * 0.25);
			world.spawnEntityInWorld(item);
		}
	}
	 
	public TileEntity createNewTileEntity(World world, int par2){
		return new TileEntitySlabChest();
	}

}
