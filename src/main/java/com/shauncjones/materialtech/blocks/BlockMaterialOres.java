package com.shauncjones.materialtech.blocks;

import java.util.List;

import com.shauncjones.materialtech.MTech;
import com.shauncjones.materialtech.moddata.Refer;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class BlockMaterialOres extends Block {

	String subBlocks[] = {"oreCopper", "oreTin", "oreSilver", "oreSilicon"};
	
	public IIcon[] icons = new IIcon[subBlocks.length];

	public BlockMaterialOres(String unlocalizedName, Material material) {
		super(material);
		this.setBlockName(unlocalizedName);
        this.setCreativeTab(MTech.MTMaterials);
        this.setHardness(2.0F);
        this.setResistance(6.0F);
        this.setStepSound(soundTypeMetal);
	}
	
	@Override
    public void registerBlockIcons(IIconRegister reg) { 
		for (int i = 0; i < subBlocks.length; i ++) {
	        this.icons[i] = reg.registerIcon(Refer.MODID + ":" + "ores_" + subBlocks[i]);
	    }
	}
	
	@Override
	public IIcon getIcon(int side, int meta) {
	    if (meta > subBlocks.length)
	        meta = 0;

	    return icons[meta];
	}
	
	@Override
	public int damageDropped(int meta) {
	    return meta;
	}
	
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list) {
	    for (int i = 0; i < subBlocks.length; i ++) {
	        list.add(new ItemStack(item, 1, i));
	    }
	}
	
}
