package com.shauncjones.materialtech.blocks.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlockWithMetadata;
import net.minecraft.item.ItemStack;

public class ItemMaterialBlocks extends ItemBlockWithMetadata{

	public ItemMaterialBlocks(Block block) {
        super(block, block);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack) {
	    return this.getUnlocalizedName() + "_" + stack.getItemDamage();
	}
	
}
