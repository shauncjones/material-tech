package com.shauncjones.materialtech.blocks.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlockWithMetadata;
import net.minecraft.item.ItemStack;

public class ItemBlocksSolarGenerators extends ItemBlockWithMetadata{

	public ItemBlocksSolarGenerators(Block block) {
        super(block, block);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack) {
	    return this.getUnlocalizedName() + "_" + stack.getItemDamage();
	}

}
