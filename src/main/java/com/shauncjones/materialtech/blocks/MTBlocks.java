package com.shauncjones.materialtech.blocks;

import com.shauncjones.materialtech.blocks.items.ItemBlocksSolarGenerators;
import com.shauncjones.materialtech.blocks.items.ItemMaterialBlocks;
import com.shauncjones.materialtech.blocks.items.ItemMaterialOreBlocks;
import com.shauncjones.materialtech.blocks.machines.BlocksMachineBlocks;
import com.shauncjones.materialtech.blocks.machines.BlocksSolarGenerators;
import com.shauncjones.materialtech.blocks.storage.BlockSlabChest;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class MTBlocks {
	
	public static Block BasicFactoryBlock;
	
	//Ores
	public static Block MaterialOres;
	
	//Material Blocks
	public static Block MaterialBlocks;
	
	//Machine Blocks
	public static Block MachineBlocks;
	public static Block SolarGenerators;
	public static Block GeneratorBlocks;
	public static Block EnergyStorageBlocks;
	
	//Storage Blocks
	public static Block slabChest;
	
	//Etc GUI Blocks
	
	//Etc Blocks
	
	public static void init(){
		
		MaterialOres = new BlockMaterialOres("materialOres", Material.rock);
		MaterialBlocks = new BlockMaterialBlocks("materialBlocks", Material.iron);
		BasicFactoryBlock = new BlockBasicFactoryBlock("basicFactoryBlock", Material.iron);
		MachineBlocks = new BlocksMachineBlocks("machineBlocks", Material.iron);	
		slabChest = new BlockSlabChest("slabChest", Material.wood);
		SolarGenerators = new BlocksSolarGenerators("solarGenerator", Material.iron);
		
		GameRegistry.registerBlock(SolarGenerators, ItemBlocksSolarGenerators.class, "solarGenerator");
		GameRegistry.registerBlock(MaterialBlocks, ItemMaterialBlocks.class, "materialBlocks");
		GameRegistry.registerBlock(MaterialOres, ItemMaterialOreBlocks.class, "materialOres");
		GameRegistry.registerBlock(BasicFactoryBlock, "basicFactoryBlock");
		GameRegistry.registerBlock(slabChest, "slabChest");
		
		blockOreDictionary();
	}

	private static void blockOreDictionary() {
		OreDictionary.registerOre("blockCopper", new ItemStack(MaterialBlocks, 0, 0));
		OreDictionary.registerOre("blockTin", new ItemStack(MaterialBlocks, 1, 1));
		OreDictionary.registerOre("blockSilver", new ItemStack(MaterialBlocks, 2, 2));
		OreDictionary.registerOre("blockSilicon", new ItemStack(MaterialBlocks, 3, 3));
		OreDictionary.registerOre("blockExplodium", new ItemStack(MaterialBlocks, 4, 4));
		OreDictionary.registerOre("blockEndento", new ItemStack(MaterialBlocks, 5, 5));
	}

}
