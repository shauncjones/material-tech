package com.shauncjones.materialtech.achieve;

import com.shauncjones.materialtech.blocks.MTBlocks;
import com.shauncjones.materialtech.items.MTItems;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.common.AchievementPage;

public class MTAchievements {
	
	public static AchievementPage MTAchievements;

	public static Achievement DiamondPick;
	public static Achievement MTStart;
	public static Achievement MTExplodium;
	
	public static void init(){
		DiamondPick = new Achievement("DiamondPickAchieve", "DiamondPickAchieve", -2, 0, Items.diamond_pickaxe, AchievementList.openInventory).registerStat().setSpecial();
		MTStart = new Achievement("MTStartAchieve", "MTStartAchieve", 0, 2, MTBlocks.MaterialOres, AchievementList.buildBetterPickaxe).registerStat();
		MTExplodium = new Achievement("MTExplodium", "MTExplodium", 2, 0, MTItems.MaterialDusts, (Achievement)null).registerStat().initIndependentStat();
		MTAchievements = new AchievementPage("Material Tech", DiamondPick, MTStart, MTExplodium);
		AchievementPage.registerAchievementPage(MTAchievements);
	}
}
