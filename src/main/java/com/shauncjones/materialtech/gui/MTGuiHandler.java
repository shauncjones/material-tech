package com.shauncjones.materialtech.gui;

import com.shauncjones.materialtech.common.tile.TileEntitySlabChest;
import com.shauncjones.materialtech.gui.container.ContainerSlabChest;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class MTGuiHandler implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = world.getTileEntity(x, y, z);
		 
        if (te != null)
        {
            if (ID == 0) //Gui ID for storage block, will add later
            {
                return new ContainerSlabChest((TileEntitySlabChest)te, player);
            }
        }
        return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity te = world.getTileEntity(x, y, z);
		 
        if (te != null)
        {
            if (ID == 0) //Gui ID for storage block, will add later
            {
                return new GuiSlabChest((TileEntitySlabChest)te, player);
            }
        }
        return null;
	}

}
