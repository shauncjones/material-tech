package com.shauncjones.materialtech.interfaces;

public interface IChunkLoadHandler {
	
	public void onChunkLoad();

}
