package com.shauncjones.materialtech;

import com.shauncjones.materialtech.core.CommonProxy;
import com.shauncjones.materialtech.items.MTItems;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

@Mod(modid = MaterialTech.MODID, name = MaterialTech.MODNAME, version = MaterialTech.VERSION)
public class MaterialTech{
	
    public static final String MODID = "materialtech";
    public static final String VERSION = "0.1.0.0";
    public static final String MODNAME = "Material Tech";
    
    @SidedProxy(clientSide="com.shauncjones.materialtech.core.ClientProxy", serverSide="com.shauncjones.materialtech.core.ServerProxy")
    public static CommonProxy proxy;
    
    public static final CreativeTabs MTMaterials = new CreativeTabs("MTMaterials") {
        @Override public Item getTabIconItem() {
            return MTItems.MaterialIngots;
        }
        @Override
        @SideOnly(Side.CLIENT)
        public int func_151243_f(){
            return 5;
        }
    };
    
    public static final CreativeTabs MTPower = new CreativeTabs("MTPower") {
        @Override public Item getTabIconItem() {
            return Items.diamond;
        }
    };
    
    public static final CreativeTabs MTStorage = new CreativeTabs("MTStorage") {
        @Override public Item getTabIconItem() {
            return Items.diamond;
        }
    };
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        proxy.preInit(e);
    }

    @EventHandler
    public void init(FMLInitializationEvent e) {
        proxy.init(e);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent e) {
        proxy.postInit(e);
    }
}
